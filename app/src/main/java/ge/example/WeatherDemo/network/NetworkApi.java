package ge.example.WeatherDemo.network;

import ge.example.WeatherDemo.model.Weather;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NetworkApi {



    @GET("/data/2.5/forecast?q=Tbilisi,us&appid="+Endpoints.APIKEY)
    Call<Weather> getWeather(@Query("q")String cityName);





}
