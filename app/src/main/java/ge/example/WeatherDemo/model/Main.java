package ge.example.WeatherDemo.model;

import com.google.gson.annotations.SerializedName;

public class Main {

        @SerializedName("temp")
        private String temp;
        @SerializedName("pressure")
        private String pressure;
        @SerializedName("humidity")
        private String humidity;
        @SerializedName("temp_min")
        private String temp_min;
        @SerializedName("temp_max")
        private String temp_max;
        @SerializedName("sea_level")
        private String sea_level;
        @SerializedName("grnd_level")
        private String grnd_level;
        @SerializedName("temp_kf")
        private String temp_kf;

    public void setGrnd_level(String grnd_level) {
        this.grnd_level = grnd_level;
    }

    public void setTemp_kf(String temp_kf) {
        this.temp_kf = temp_kf;
    }

    public void setSea_level(String sea_level) {
        this.sea_level = sea_level;
    }

    public void setTemp(String temp) {
            this.temp = temp;
        }

        public void setPressure(String pressure) {
            this.pressure = pressure;
        }

        public void setHumidity(String humidity) {
            this.humidity = humidity;
        }

        public void setTemp_max(String temp_max) {
            this.temp_max = temp_max;
        }

        public void setTemp_min(String temp_min) {
            this.temp_min = temp_min;
        }

        public String getTemp() {
            return temp;
        }

        public String getHumidity() {
            return humidity;
        }

        public String getPressure() {
            return pressure;
        }

        public String getTemp_max() {
            return temp_max;
        }

        public String getTemp_min() {
            return temp_min;
        }

    public String getSea_level() {
        return sea_level;
    }

    public String getGrnd_level() {
        return grnd_level;
    }

    public String getTemp_kf() {
        return temp_kf;
    }

}
