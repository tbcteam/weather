package ge.example.WeatherDemo.model;

import com.google.gson.annotations.SerializedName;

class Snow {
    @SerializedName("3h")
    private String h;

    public void setH(String h) {
        this.h = h;
    }

    public String getH() {
        return h;
    }
}
