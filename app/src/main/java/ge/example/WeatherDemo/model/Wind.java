package ge.example.WeatherDemo.model;

import com.google.gson.annotations.SerializedName;

class Wind {
    @SerializedName("speed")
    private String speed;
    @SerializedName("deg")
    private String deg;
}
