package ge.example.WeatherDemo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Weather {
    @SerializedName("cod")
    private String cod;
    @SerializedName("message")
    private String message;
    @SerializedName("cnt")
    private String cnt;
    @SerializedName("list")
    List<WList> wlist;

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public void setWlist(List<WList> wlist) {
        this.wlist = wlist;
    }

    public String getMessage() {
        return message;
    }

    public List<WList> getWlist() {
        return wlist;
    }

    public String getCod() {
        return cod;
    }

    public String getCnt() {
        return cnt;
    }

}
