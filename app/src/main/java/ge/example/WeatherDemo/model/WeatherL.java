package ge.example.WeatherDemo.model;

import com.google.gson.annotations.SerializedName;

public class WeatherL {
    @SerializedName("id")
    private String id;
    @SerializedName("main")
    private String main;
    @SerializedName("description")
    private String description;
    @SerializedName("icon")
    private String icon;

    public void setMain(String main) {
        this.main = main;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public String getDescription() {
        return description;
    }

    public String getMain() {
        return main;
    }

    public String getId() {
        return id;
    }
}
