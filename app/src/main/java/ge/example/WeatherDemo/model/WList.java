package ge.example.WeatherDemo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WList {

    @SerializedName("dt")
    private String dt;

    @SerializedName("weather")
    List<WeatherL> weathers;

    @SerializedName("main")
    private Main main;

    @SerializedName("clouds")
    private Clouds clouds;

    @SerializedName("wind")
    private Wind wind;

    @SerializedName("snow")
    private Snow snow;

    @SerializedName("sys")
    private Sys sys;

    @SerializedName("dt_txt")
    private String date_txt;


    public Main getWeather() {
        return main;
    }

    public void setWeather(Main main) {
        this.main = main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public void setDate_txt(String date_txt) {
        this.date_txt = date_txt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public void setSnow(Snow snow) {
        this.snow = snow;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public void setWeathers(List<WeatherL> weathers) {
        this.weathers = weathers;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public List<WeatherL> getWeathers() {
        return weathers;
    }

    public Main getMain() {
        return main;
    }

    public Snow getSnow() {
        return snow;
    }

    public String getDt() {
        return dt;
    }

    public String getDate_txt() {
        return date_txt;
    }

    public Sys getSys() {
        return sys;
    }

    public Wind getWind() {
        return wind;
    }
}
