package ge.example.WeatherDemo.model;

import com.google.gson.annotations.SerializedName;

public class PhotoModel {
    @SerializedName("name")
    private String uname;

    @SerializedName("phone")
    private String uphone;

    @SerializedName("company")
    private Company company;

    @SerializedName("address")
    private Address address;

    @SerializedName("id")
    private String id;



    public String getId() {
        return id;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public String getname() {
        return uname;
    }

    public String getPhone() {
        return uphone;
    }

    public Company getCompany() {
        return  company;
    }


    public void setUname(String uname) {
        this.uname = uname;
    }



    public void setUphone(String uphone) {
        this.uphone = uphone;
    }

    public void setUcompany(Company companyy) {
        this.company = companyy;
    }

    public class Company{
        @SerializedName("name")
       private  String companyName;

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCompanyName() {
            return companyName;
        }
    }
    public class Address{
        @SerializedName("geo")
        private Geo geo;

        public void setGeo(Geo geo) {
            this.geo = geo;
        }

        public Geo getGeo() {
            return geo;
        }
    }

    public class Geo{
        @SerializedName("lat")
        private String lat;
        @SerializedName("lng")
        private String lng;

        public void setLng(String lng) {
            this.lng = lng;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public String getLat() {
            return lat;
        }
    }






}

