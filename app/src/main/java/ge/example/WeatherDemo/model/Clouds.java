package ge.example.WeatherDemo.model;

import com.google.gson.annotations.SerializedName;

class Clouds {
    @SerializedName("all")
    private String all;

    public void setAll(String all) {
        this.all = all;
    }

    public String getAll() {
        return all;
    }
}
