package ge.example.WeatherDemo.model;

import com.google.gson.annotations.SerializedName;

class Sys {
    @SerializedName("pod")
    private String pod;

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }
}
