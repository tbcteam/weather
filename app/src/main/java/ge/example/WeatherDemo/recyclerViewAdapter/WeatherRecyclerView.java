package ge.example.WeatherDemo.recyclerViewAdapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import java.util.List;

import ge.example.WeatherDemo.model.WList;
import ge.geolab.WeatherDemo.R;

public class WeatherRecyclerView extends RecyclerView.Adapter<WeatherRecyclerView.ViewHolder> {
    private List<WList> wLists;
    private LayoutInflater mInflater;
    OnItemClickListener onItemClickListener;
    Context context;

    int firstDay=0;


    public WeatherRecyclerView(Context context, List<WList> wLists) {
        this.mInflater = LayoutInflater.from(context);
        this.context=context;
        this.wLists=wLists;
    }
    public void setDate(List<WList> wLists){
        this.wLists=wLists;
        notifyDataSetChanged();
    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener=onItemClickListener;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.recyclerview_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.date.setText(wLists.get(i).getDate_txt());
        viewHolder.temperature.setText(String.format("%sF", (int)Double.parseDouble(wLists.get(i).getWeather().getTemp())));
        viewHolder.humidity.setText(String.format("%s", wLists.get(i).getMain().getHumidity()));
        if(wLists.get(i).getWeathers().get(0).getMain().equals("Clear")) {
            viewHolder.image.setImageResource(R.drawable.clear);
        }else
            if(wLists.get(i).getWeathers().get(0).getMain().equals("Snow")){
        viewHolder.image.setImageResource(R.drawable.snow);}
            else {
                viewHolder.image.setImageResource(R.drawable.cloud);
            }

    }

    @Override
    public int getItemCount() {
        return wLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView date;
        TextView temperature;
        TextView humidity;
        ImageView image;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date_id);
            temperature=itemView.findViewById(R.id.temperature_id);
            humidity=itemView.findViewById(R.id.humidity_id);
            image=itemView.findViewById(R.id.image_id);
            cardView=itemView.findViewById(R.id.card_id);
            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(onItemClickListener!=null)
            onItemClickListener.onItemClick(v,wLists.get(getAdapterPosition()));
        }
    }
    public interface OnItemClickListener{
       void onItemClick(View v,WList wList);
    }
}
