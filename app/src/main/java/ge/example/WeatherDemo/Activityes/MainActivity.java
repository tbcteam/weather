package ge.example.WeatherDemo.Activityes;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ge.example.WeatherDemo.Exceptions.NoInternetConnectionException;
import ge.example.WeatherDemo.Exceptions.NoWeatherByCityNameException;
import ge.example.WeatherDemo.utils.DefaultCityNames;
import ge.example.WeatherDemo.model.WList;
import ge.example.WeatherDemo.model.Weather;
import ge.example.WeatherDemo.utils.ConnectionStatus;
import ge.example.WeatherDemo.network.NetworkManager;
import ge.example.WeatherDemo.recyclerViewAdapter.WeatherRecyclerView;
import ge.geolab.WeatherDemo.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements WeatherRecyclerView.OnItemClickListener,View.OnClickListener{

    public static final String ERRORTAG="weather_errors";
    public static final String INTENTKEY = "key.details.weather";
    public static final String CITYNAMEKEY="key.city.name";


    Button tbilisi;
    Button kutaisi;
    Button batumi;


    SearchView searchView;
    WeatherRecyclerView adapter;
    ProgressBar progressBar;
    CardView noInternetCardView;

    String cityName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                changeButtonsDefaultColors();
                String name=s.toLowerCase();
                getWeatherByCityName(s.toLowerCase());
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }
    @Override
    protected void onResume(){
        super.onResume();
        getWeatherByCityName(DefaultCityNames.SUPERDEFAULT);
    }

    private void init(){
       tbilisi=findViewById(R.id.tbilisi_btn);
       kutaisi=findViewById(R.id.kutaisi_btn);
       batumi=findViewById(R.id.batumi_btn);
       tbilisi.setOnClickListener(this);
       kutaisi.setOnClickListener(this);
       batumi.setOnClickListener(this);
        searchView=findViewById(R.id.search_id);
        progressBar=findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        noInternetCardView=findViewById(R.id.no_internet_card_view);
        noInternetCardView.setVisibility(View.GONE);
        RecyclerView recyclerView = findViewById(R.id.weather_recycler_view_id);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<WList> wLists = new ArrayList<>();
        adapter = new WeatherRecyclerView(MainActivity.this, wLists);
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
    }

   private List<WList> filter(List<WList> wLists){
        List<WList> res=new ArrayList<>();
        int date=0;
      for(int i=0;i<wLists.size();i++){
        if(date!=Integer.parseInt(wLists.get(i).getDate_txt().substring(8,10))){
            date=Integer.parseInt(wLists.get(i).getDate_txt().substring(8,10));
            wLists.get(i).setDate_txt(wLists.get(i).getDate_txt().substring(0,10));
            res.add(wLists.get(i));

        }
      }
      return res;
   }
   private void getWeather(String cityName){
        progressBar.setVisibility(View.VISIBLE);
       Call<Weather> req = NetworkManager.getApi().getWeather(cityName);
       req.enqueue(new Callback<Weather>() {
           @Override
           public void onResponse(Call<Weather> call, Response<Weather> response) {
               if(response.isSuccessful()){
                   progressBar.setVisibility(View.GONE);
                   noInternetCardView.setVisibility(View.GONE);
                   Weather  weathers=response.body();
                   adapter.setDate(filter(Objects.requireNonNull(weathers).getWlist()));
                    // ActivityMainBinding binding = DataBindingUtil.setContentView(MainActivity.this, R.layout.activity_main);
                   //  binding.setWeather(weathers);
               }
           }
           @Override
           public void onFailure(Call<Weather> call, Throwable t) {
               Log.d(ERRORTAG, "onFailure: " + t.getMessage());
               progressBar.setVisibility(View.GONE);
               if (!ConnectionStatus.isInternetConnection(getApplicationContext())) {
                   try {
                       throw new NoInternetConnectionException();
                   } catch (NoInternetConnectionException e) {
                       Log.d(ERRORTAG, "onFailure: " + e.getMessage());
                       noInternetCardView.setVisibility(View.VISIBLE);
                   }
               } else {
                   try {
                       throw new NoWeatherByCityNameException(cityName);
                   } catch (NoWeatherByCityNameException e) {
                       Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                       Log.d(ERRORTAG, "onFailure: " + e.getMessage());
                   }

               }
           }
       });
   }

    @Override
    public void onItemClick(View v, WList wList) {
        Intent details=new Intent(MainActivity.this,DetailWeatherActivity.class);
        details.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        details.putExtra(INTENTKEY,wList.getDate_txt());
        details.putExtra(CITYNAMEKEY,cityName);
        startActivity(details);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tbilisi_btn:
                getWeatherByCityName(DefaultCityNames.TBILISI);
                break;
            case R.id.kutaisi_btn:
                getWeatherByCityName(DefaultCityNames.KUTAISI);
                break;
            case R.id.batumi_btn:
                getWeatherByCityName(DefaultCityNames.BATUMI);
                break;
            default:
                break;
        }
    }
    private void changeButtonsDefaultColors() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tbilisi.setBackgroundColor(this.getColor(R.color.colorDefaultButton));
            kutaisi.setBackgroundColor(this.getColor(R.color.colorDefaultButton));
            batumi.setBackgroundColor(this.getColor(R.color.colorDefaultButton));
        }
    }
    public void getWeatherByCityName(String id){
        cityName=id.toUpperCase();
        switch(id){
            case "tbilisi":
                changeButtonsDefaultColors();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tbilisi.setBackgroundColor(this.getColor(R.color.colorBlue));
                }
                DefaultCityNames.SUPERDEFAULT=DefaultCityNames.TBILISI;
                getWeather(id);
                break;
            case "kutaisi":
                changeButtonsDefaultColors();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    kutaisi.setBackgroundColor(this.getColor(R.color.colorBlue));
                }
                DefaultCityNames.SUPERDEFAULT=DefaultCityNames.KUTAISI;
                getWeather(id);
                break;
            case "batumi":
                changeButtonsDefaultColors();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    batumi.setBackgroundColor(this.getColor(R.color.colorBlue));
                }
                DefaultCityNames.SUPERDEFAULT=DefaultCityNames.BATUMI;
                getWeather(id);
                break;
            default:
                changeButtonsDefaultColors();
                DefaultCityNames.SUPERDEFAULT=id;
                getWeather(id);
                break;
        }
    }
}


