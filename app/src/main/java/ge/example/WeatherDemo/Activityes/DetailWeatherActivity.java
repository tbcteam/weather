package ge.example.WeatherDemo.Activityes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ge.example.WeatherDemo.Exceptions.NoInternetConnectionException;
import ge.example.WeatherDemo.Exceptions.NoWeatherByCityNameException;
import ge.example.WeatherDemo.model.WList;
import ge.example.WeatherDemo.model.Weather;
import ge.example.WeatherDemo.utils.ConnectionStatus;
import ge.example.WeatherDemo.network.NetworkManager;
import ge.example.WeatherDemo.recyclerViewAdapter.WeatherRecyclerView;
import ge.geolab.WeatherDemo.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ge.example.WeatherDemo.Activityes.MainActivity.ERRORTAG;

public class DetailWeatherActivity extends AppCompatActivity {

    private String date;
    private String cityName;

    ProgressBar progressBar;
    CardView noInternetCardView;
    WeatherRecyclerView adapter;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_weather);
        init();
        toolbarClick();
        getWeather(cityName);

    }

    private List<WList> filterByDate(List<WList> wLists, String date) {
        List<WList> result = new ArrayList<>();
        for (int i = 0; i < wLists.size(); i++) {
            if (wLists.get(i).getDate_txt().substring(0, 10).equals(date.substring(0, 10))) {
                wLists.get(i).setDate_txt(wLists.get(i).getDate_txt().substring(10,16));
                result.add(wLists.get(i));
            }
        }
        return result;
    }

    private void getWeather(String cityName) {
        progressBar.setVisibility(View.VISIBLE);
        Call<Weather> req = NetworkManager.getApi().getWeather(cityName);
        req.enqueue(new Callback<Weather>() {
            @Override
            public void onResponse(Call<Weather> call, Response<Weather> response) {
                Log.d("infofilter", "onResponse: ");
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    noInternetCardView.setVisibility(View.GONE);
                    Weather weathers = response.body();
                   List<WList> wLists=filterByDate(Objects.requireNonNull(weathers).getWlist(),date);
                    adapter.setDate(wLists);
                    // ActivityMainBinding binding = DataBindingUtil.setContentView(MainActivity.this, R.layout.activity_main);
                    //  binding.setWeather(weathers);
                    Log.d("weather", "onResponse: " + weathers.getWlist().get(0).getDate_txt());
                }
            }

            @Override
            public void onFailure(Call<Weather> call, Throwable t) {
                Log.d(ERRORTAG, "onFailure: " + t.getMessage());
                progressBar.setVisibility(View.GONE);
                if (!ConnectionStatus.isInternetConnection(getApplicationContext())) {
                    try {
                        throw new NoInternetConnectionException();
                    } catch (NoInternetConnectionException e) {
                        Log.d(ERRORTAG, "onFailure: " + e.getMessage());
                        noInternetCardView.setVisibility(View.VISIBLE);
                    }
                } else {
                    try {
                        throw new NoWeatherByCityNameException(cityName);
                    } catch (NoWeatherByCityNameException e) {
                        Toast.makeText(DetailWeatherActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d(ERRORTAG, "onFailure: " + e.getMessage());
                    }

                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        toolbar.setTitle(cityName);
    }

    private void init(){

        date = getIntent().getStringExtra(MainActivity.INTENTKEY);
        cityName=getIntent().getStringExtra(MainActivity.CITYNAMEKEY);

        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressBar=findViewById(R.id.detail_progressBar);
        progressBar.setVisibility(View.GONE);
        noInternetCardView=findViewById(R.id.detail_no_internet_card_view);
        noInternetCardView.setVisibility(View.GONE);
        RecyclerView recyclerView = findViewById(R.id.detail_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<WList> wLists = new ArrayList<>();
        adapter = new WeatherRecyclerView(DetailWeatherActivity.this, wLists);
      //  adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);


    }
private void toolbarClick(){
        toolbar.setNavigationOnClickListener(v -> {
            startActivity(new Intent(DetailWeatherActivity.this, MainActivity.class)
            .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            finish();
        });
}

}

