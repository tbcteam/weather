package ge.example.WeatherDemo.Exceptions;

public class NoInternetConnectionException extends Exception {
    private String error="გთხოვთ ჩაართეთ ინტერნეტი!!!";

    @Override
    public String getMessage() {
        return error;
    }

}
