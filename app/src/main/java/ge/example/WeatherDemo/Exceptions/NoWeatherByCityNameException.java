package ge.example.WeatherDemo.Exceptions;

public class NoWeatherByCityNameException extends Exception {
    public NoWeatherByCityNameException(String name){
        super(name+": არასწორი ქალაქის სახელია!!!");
    }
}
